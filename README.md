<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Cuisines Registry](#cuisines-registry)
    - [The story:](#the-story)
    - [Your tasks:](#your-tasks)
    - [Submitting your solution](#submitting-your-solution)
    - [Solution and suggestions](#solution-and-suggestions)
        - [Your task explanation](#your-task-explanation)
        - [Test](#test)
        - [lombok](#lombok)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## Solution and suggestions

### 

### "Your task" explanation
1. Javadocs are added to CuisinesRegistry. It was not changed as interface with methods and parameters of the methods.
2. New solution covers multiple cuisines for a customer. The proof is at de.quandoo.recruitment.registry.InMemoryCuisinesRegistryTest#testMultiCuisineSupport
3. New solutions covers returning a list of most popular cuisines. The proof is at de.quandoo.recruitment.registry.InMemoryCuisinesRegistryTest#testTopCuisines
4. For billions data in-memory:
    - JVM and data can be separated. JVM has a maximum memory limit. 
    - The identifiers of the models are String. The text areas need more memory size. The identifiers can replaced with a number to save memory.
    - Using in-memory-cache means you can lose your data If your app is down. The data must be persisted. There are some solutions to persist data like relational databases, NoSqls or in-memory solutions. Relational databases are very slow for huge data. NoSqls are very fast but not in-memory. NoSqls are still really good solutions. If the in-memory is must, I can offer Redis or Hazelcast.
     
### Test
Coverage: %100, Line Coverage: 52/52

### lombok

- For eclipse: [link1](https://howtodoinjava.com/automation/lombok-eclipse-installation-examples/) and [link2](https://projectlombok.org/setup/eclipse)
- For intellij: [link1](https://projectlombok.org/setup/intellij) and [link2](https://github.com/mplushnikov/lombok-intellij-plugin)