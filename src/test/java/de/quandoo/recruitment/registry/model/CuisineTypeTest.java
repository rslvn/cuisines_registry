package de.quandoo.recruitment.registry.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * Created by resulav on 25.07.2018.
 */
public class CuisineTypeTest {

    @Test
    public void testOf(){
        Optional<CuisineType> optional = CuisineType.of(CuisineType.FRENCH.getName());
        Assert.assertTrue("CuisineType not found by name",optional.isPresent());
    }

    @Test
    public void testOfEmpty(){
        Optional<CuisineType> optional = CuisineType.of("noCuisineType");
        Assert.assertFalse("CuisineType found by name",optional.isPresent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOfNoText(){
        CuisineType.of("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOfNull(){
        CuisineType.of(null);
    }
}
