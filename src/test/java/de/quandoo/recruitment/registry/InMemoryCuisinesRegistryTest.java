package de.quandoo.recruitment.registry;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineType;
import de.quandoo.recruitment.registry.model.Customer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InMemoryCuisinesRegistryTest {

	private static final String ERROR_CUISINE_LIST_NULL = "cuisineList can not be null";
	private static final String ERROR_CUISINE_LIST_SIZE_MISMATCHED = "cuisineList size mismatched";
	private static final String ERROR_CUSTOMER_S_CUISINE_NOT_MATCHED = "customer's cuisine not matched";

	private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
	private Customer customer1;
	private Customer customer2;
	private Customer customer3;

	private Cuisine french;
	private Cuisine german;
	private Cuisine italian;

	@Before
	public void setup() {
		customer1 = Customer.builder().uuid("1").build();
		customer2 = Customer.builder().uuid("2").build();
		customer3 = Customer.builder().uuid("3").build();

		french = Cuisine.builder().name(CuisineType.FRENCH.getName()).build();
		german = Cuisine.builder().name(CuisineType.GERMAN.getName()).build();
		italian = Cuisine.builder().name(CuisineType.ITALIAN.getName()).build();
	}

	/**
	 * register test </br>
	 * validating by cuisine
	 */
	@Test
	public void shouldWork1() {
		cuisinesRegistry.register(customer1, french);

		List<Customer> customerList = cuisinesRegistry.cuisineCustomers(french);
		Assert.assertNotNull("customerList can not be null", customerList);
		Assert.assertTrue("customerList size mismatched", customerList.size() == 1);
		Assert.assertEquals("cuisine's customer not matched", customerList.get(0), customer1);
	}

	/**
	 * register test </br>
	 * validating by customer
	 */
	@Test
	public void testCustomerCuisines() {
		cuisinesRegistry.register(customer2, german);
		List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(customer2);

		Assert.assertNotNull(ERROR_CUISINE_LIST_NULL, cuisineList);
		Assert.assertEquals(ERROR_CUISINE_LIST_SIZE_MISMATCHED, 1, cuisineList.size());
		Assert.assertEquals(ERROR_CUSTOMER_S_CUISINE_NOT_MATCHED, cuisineList.get(0), german);
	}

	/**
	 * null Cuisine Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void shouldWork2() {
		cuisinesRegistry.cuisineCustomers(null);
	}

	/**
	 * null Customer Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void shouldWork3() {
		cuisinesRegistry.customerCuisines(null);
	}

	/**
	 * null Cuisine name Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCuisineNullNameTest() {
		cuisinesRegistry.cuisineCustomers(Cuisine.builder().build());
	}

	/**
	 * empty Cuisine name Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCuisineEmptyNameTest() {
		cuisinesRegistry.cuisineCustomers(Cuisine.builder().name("").build());
	}

	/**
	 * null Customer uuid Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCustomerUUIDNull() {
		cuisinesRegistry.customerCuisines(Customer.builder().uuid(null).build());
	}

	/**
	 * null Customer uuid Test
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCustomerUUIDEmpty() {
		cuisinesRegistry.customerCuisines(Customer.builder().uuid("").build());
	}

	/**
	 * top popular cuisine test
	 */
	@Test
	public void shouldWork4() {
		int limit = 1;
		List<Cuisine> cuisineList = cuisinesRegistry.topCuisines(limit);
		Assert.assertNotNull(ERROR_CUISINE_LIST_NULL, cuisineList);
		Assert.assertEquals(ERROR_CUISINE_LIST_SIZE_MISMATCHED, cuisineList.size(), limit);
	}

	/**
	 * multi cuisine support test
	 */
	@Test
	public void testMultiCuisineSupport() {
		cuisinesRegistry.register(customer1, german);
		cuisinesRegistry.register(customer1, italian);

		List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(customer1);
		Assert.assertNotNull(ERROR_CUISINE_LIST_NULL, cuisineList);
		Assert.assertEquals(ERROR_CUISINE_LIST_SIZE_MISMATCHED, 3, cuisineList.size());
	}

	/**
	 * This is the last test by name. Final view is below: </br>
	 * 3 customers like french cuisine </br>
	 * 2 customers like german cuisine </br>
	 * 1 customer likes italian cuisine
	 * 
	 */
	@Test
	public void testTopCuisines() {
		cuisinesRegistry.register(customer2, french);

		cuisinesRegistry.register(customer3, french);

		List<Cuisine> cuisineList = cuisinesRegistry.topCuisines(3);
		Assert.assertNotNull(ERROR_CUISINE_LIST_NULL, cuisineList);
		Assert.assertEquals(ERROR_CUISINE_LIST_SIZE_MISMATCHED, 3, cuisineList.size());
		Assert.assertEquals(ERROR_CUSTOMER_S_CUISINE_NOT_MATCHED, french, cuisineList.get(0));
		Assert.assertEquals(ERROR_CUSTOMER_S_CUISINE_NOT_MATCHED, german, cuisineList.get(1));
		Assert.assertEquals(ERROR_CUSTOMER_S_CUISINE_NOT_MATCHED, italian, cuisineList.get(2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testTopCuisinesInvalidLimit() {
		cuisinesRegistry.topCuisines(0);
	}

}