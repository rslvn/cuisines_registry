/**
 * 
 */
package de.quandoo.recruitment.registry.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

/**
 * The enum is created for speeding up name to Cuisine conversion
 * 
 * @author resulav
 *
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CuisineType {
	ITALIAN("italian"), FRENCH("french"), GERMAN("german"),;
	@NonNull
	private final String name;

	private static final Map<String, CuisineType> typeMap;
	static {
		typeMap = new HashMap<>(CuisineType.values().length);
		for (CuisineType cuisineType : CuisineType.values()) {
			typeMap.put(cuisineType.name, cuisineType);
		}
	}

	public static Optional<CuisineType> of(final String name) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "name can not be null or empty");
		CuisineType cuisineType = typeMap.get(name.toLowerCase());
		if (cuisineType != null) {
			return Optional.of(cuisineType);
		}
		return Optional.empty();
	}
}