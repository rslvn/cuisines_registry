package de.quandoo.recruitment.registry.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class Customer {
	private final String uuid;
}
