package de.quandoo.recruitment.registry;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineType;
import de.quandoo.recruitment.registry.model.Customer;

/**
 * The class is created for in memory caching of cuisines and their registered
 * customers
 * 
 * @author resulav
 *
 */
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	private static final Cache<CuisineType, Set<Customer>> cuisineCache = CacheBuilder.newBuilder().build();
	private static final Cache<Customer, Set<CuisineType>> customerCache = CacheBuilder.newBuilder().build();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.quandoo.recruitment.registry.api.CuisinesRegistry#register(de.quandoo.
	 * recruitment.registry.model.Customer,
	 * de.quandoo.recruitment.registry.model.Cuisine)
	 */
	@Override
	public void register(final Customer userId, final Cuisine cuisine) {
		validateCustomer(userId);
		CuisineType cuisineType = validateCuisineAndGetType(cuisine);
		cuisineCache.asMap().computeIfAbsent(cuisineType, key -> Sets.newConcurrentHashSet()).add(userId);
		customerCache.asMap().computeIfAbsent(userId, key -> Sets.newConcurrentHashSet()).add(cuisineType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.quandoo.recruitment.registry.api.CuisinesRegistry#cuisineCustomers(de.
	 * quandoo.recruitment.registry.model.Cuisine)
	 */
	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		CuisineType cuisineType = validateCuisineAndGetType(cuisine);
		return cuisineCache.asMap().getOrDefault(cuisineType, Sets.newHashSet()).stream().collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines(de.
	 * quandoo.recruitment.registry.model.Customer)
	 */
	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		validateCustomer(customer);
		return customerCache.asMap()
				.getOrDefault(customer, Sets.newHashSet())
				.stream()
				.map(c -> Cuisine.builder().name(c.getName()).build())
				.collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines(int)
	 */
	@Override
	public List<Cuisine> topCuisines(final int n) {
		Preconditions.checkArgument(n > 0, "limit not valid");
		return cuisineCache.asMap().entrySet().stream()
				.sorted(Map.Entry.comparingByValue((o1, o2) -> Integer.compare(o2.size(), o1.size())))
				.limit(n)
				.map(e -> Cuisine.builder().name(e.getKey().getName()).build())
				.collect(Collectors.toList());
	}

	/**
	 * The validator of {@link Customer}
	 * 
	 * @param customer the customer to validate
	 */
	private void validateCustomer(Customer customer) {
		Preconditions.checkArgument(customer != null, "customer can not be null");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(customer.getUuid()),
				"customer.uuid can not be null or empty");
	}

	/**
	 * The validator of {@link Cuisine}
	 * 
	 * @param cuisine the cuisine to validate
	 * @return a valid {@link CuisineType}
	 */
	private CuisineType validateCuisineAndGetType(Cuisine cuisine) {
		Preconditions.checkArgument(cuisine != null, "cuisine can not be null");
		Preconditions.checkArgument(!Strings.isNullOrEmpty(cuisine.getName()), "cuisine.name can not be null or empty");
		Optional<CuisineType> cuisineTypeOpt = CuisineType.of(cuisine.getName());
		Preconditions.checkArgument(cuisineTypeOpt.isPresent(),
				"Unknown cuisine, please reach johny@bookthattable.de to update the code");

		return cuisineTypeOpt.get();
	}
}
