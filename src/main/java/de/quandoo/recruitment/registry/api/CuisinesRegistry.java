package de.quandoo.recruitment.registry.api;

import java.util.List;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public interface CuisinesRegistry {

	/**
	 * register a customer to a cuisine.
	 * 
	 * @param customer the customer
	 * @param cuisine  the cuisine
	 */
	void register(Customer customer, Cuisine cuisine);

	/**
	 * returns the customer's registered cuisine
	 * 
	 * @param customer the customer
	 * @return list of {@link Cuisine}
	 */
	List<Cuisine> customerCuisines(Customer customer);

	/**
	 * returns the most popular cuisines
	 * 
	 * @param n limit of the cuisines
	 * @return list of {@link Cuisine}
	 */
	List<Cuisine> topCuisines(int n);

	/**
	 * returns registered customers of the cuisine
	 * 
	 * @param cuisine the cuisine
	 * @return list of {@link Customer}
	 */
	List<Customer> cuisineCustomers(Cuisine cuisine);
}
